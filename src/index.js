'use strict'

const my_shared_code_headless = require('./resource/js/my_shared_code_headless')

const express = require('express')
const app = express()
const bodyParser = require('body-parser')
//Déboguer les routes de votre application
const morgan = require('morgan')
app.use(morgan('dev'))

app.use(bodyParser.json())


// …
let even_numbers = my_shared_code_headless.generateEvenNumbers(25)
console.log('even_numbers:', even_numbers)

app.use(express.static('public'))

app.get('/even_numbers', function (req, res) {
  res.send(even_numbers)
})



//methode Get
app.get('/api', function (req, res) {
  res.send({fruits : [
    { ref : "ref-jaune",
      nom : "banane"
    },
    { ref : "ref-rouge",
      nom : "fraise"
    }
  ]})
})


//Methode Post
app.post('/api',function(req, res){
  console.log("Donnée à Poster :", req.body)
  res.send(req.body.donne1)
})


//methode delet
app.delete('/api',function(req,res){
  console.log("Donnée à Supprimer :",req.query.donne2)
  req.query.donne2=' '
  res.send('bien supprimer')
});

//methode put
app.put('/api', function (req, res)
{
  console.log("Donnée à Modifier :",req.body);

    if(req.body.donne2!=='')
    {
        res.send('Modifier');
    }
    else
    {
      res.send("Element vide !!");
    }
});

//communication avec desktop
app.post('/api/chemin',function(req, res){
  console.log(req.body)
  res.send({success:true})
})

app.listen(3000, function () {
  console.log('Example app listening on port 3000!')
})
